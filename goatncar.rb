# frozen_string_literal: true

require_relative 'lib/game'

class GoatNCar
  def initialize(trial_number = 1000, verbose: false)
    @trial_number = trial_number
    @verbose      = verbose
  end

  def run
    @trial_number.times.map do |i|
      game = Game.new

      if @verbose
        puts "Trial##{i}: #{game.set}"
        puts "  #{game.guesses_and_clue}"
        puts "  Winner: #{game.winner}"
      end

      game.winner
    end.each_with_object(Hash.new(0)) do |winner, results|
      results[winner] += 1
    end.then(&method(:format_results))
  end

  def format_results(results)
    puts if @verbose
    +"FINAL RESULTS:\n" <<
      results.sort.map do |guess_name, result|
        "  #{guess_name}: #{result}/#{@trial_number}" \
        " (#{(result / @trial_number.to_f * 100).round(2)}%)"
      end.join("\n")
  end
end

puts GoatNCar.new(verbose: true).run
