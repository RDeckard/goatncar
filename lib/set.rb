# frozen_string_literal: true

class Set
  attr_reader :boxes

  def initialize(box_contents = {})
    @boxes = []

    box_contents.each do |box_content, number|
      number.times { @boxes << box_content }
    end

    @boxes.shuffle!
  end

  def to_s
    +'Set: ' <<
      @boxes.map.with_index do |box_content, i|
        "#{box_content}(#{i})"
      end.join(', ')
  end
end
