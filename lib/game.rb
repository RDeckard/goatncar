# frozen_string_literal: true

require_relative 'set'

class Game
  BOX_CONTENTS = {
    goat: 2,
    car:  1
  }.freeze

  GOOD_CONTENT = :car
  CLUE_CONTENT = :goat

  attr_reader :set

  def initialize
    @set = Set.new(BOX_CONTENTS)
  end

  def winner
    @set.boxes.find_index { |box_content| box_content == GOOD_CONTENT }.
      then do |good_index|
        guesses_and_clue.find { |_name, i| i == good_index }
      end.first
  end

  def guesses_and_clue
    @guesses_and_clue ||=
      {
        first_guess: first_guess_index,
        clue:        clue_index,
        new_guess:   new_guess_index
      }
  end

  def first_guess_index
    @first_guess_index ||= rand(index_range)
  end

  def clue_index
    @clue_index ||=
      @set.boxes.map.with_index.find do |box_content, i|
        i != first_guess_index && box_content == CLUE_CONTENT
      end.last
  end

  def new_guess_index
    @new_guess_index ||=
      index_range.find { |i| ![first_guess_index, clue_index].include?(i) }
  end

  private

  def index_range
    @index_range ||=
      0..(@set.boxes.count - 1)
  end
end
